#include "tp10.h"

void afficherDate(Date date) { //affiche la date ("lireDate")
    cout << date.jour << " " << date.mois << " " << date.annee << "." << endl;
}

void saisirDate(Date *date) { //demande une date à l'utilisateur ("ecrireDate")
    int j, a;
    string m;
    cout << "jour ? " ;
    cin >> j;
    cout << "mois ? " ;
    cin >> m;
    cout << "annee ? " ;
    cin >> a;

    (*date).jour = j;
    (*date).mois = m;
    (*date).annee = a;
}


void saisirFiche(Fiche *fiche) { //demande à l'utilosateur d'entrer une fiche
    string nom ;
    string prenom;
    Date anniv;
    int nbNotes = 0; //initialisation 

    cout << "nom ? " ;
    cin >> nom;
    cout << "prénom ? " ;
    cin >>  prenom;
    cout << "Date de naissance : " ;
    saisirDate(&anniv); //on demande les éléments de la fiche à l'utilisateur

    (*fiche).nom = nom;
    (*fiche).prenom = prenom;
    (*fiche).dateDeNaissance = anniv;
    (*fiche).nbNotes = nbNotes; //affectation
}

void ajoutNote(Fiche *fiche) {
    float note; //la note qu'on va ajouter

    if ((*fiche).nbNotes == MAXNOTES) { //si le tableau est déjà plein, on ne fait rien.
        return ;
    }
    else {
        cout << "Nouvelle note : " ;
        cin >> note ; //on demande une nouvelle note
        (*fiche).notes[(*fiche).nbNotes] = note; //on l'ajoute en fin de tableau
        (*fiche).nbNotes++; //on incrémente le nombre de notes de la fiche
        }
    }

void afficherFiche(Fiche fiche) {
    //affichage de tous les éléments de la fiche
    cout <<"Nom : " << fiche.nom << endl;
    cout <<"Prénom : " << fiche.prenom << endl;
    cout <<"Date de naissance : " ;
    afficherDate(fiche.dateDeNaissance);
    cout <<"Notes : " ; 
    
    if (fiche.nbNotes == 0) {
        cout << "aucune note." << endl;
        return;
    }
    else {
        for (int i=0; i<fiche.nbNotes; i++) { //affichage des notes en parcourant le tableau notes
            cout << fiche.notes[i] << " " ;
        }
    }
}

void initialiserClasse(Classe *classe) {
//cette fonction ne fait qu'allouer de la mémoire à la future classe, elle ne rentre aucune fiche

    int t = 0; //init Taille
    int c = 0; //init Capacité

    cout << "Taille de la Classe ?" << endl;
    cin >> t;
    classe->taille = t;

    cout << "Capacité de la Classe ?" << endl;
    cin >> c ;
    classe->capacite = c;


    if (new Fiche[c]==nullptr) { //valeur que prend new si l'allocation échoue = pointeur nul
        cout << "-1 : ERREUR D'ALLOCATION" << endl; //code d'erreur
        }

    else {
        Fiche *fiches = new Fiche[c]; //allocat° dynamique des fiches : on crée l'espace c fiches 
        classe->fiches = fiches; 
     }
}



bool dejaPresente(Classe classe, Fiche fiche) {
    int c = classe.capacite; //capacité de la classe passée en argument
    for (int i=0; i<=c; i++) {
        if (classe.fiches[i].nom==fiche.nom && classe.fiches[i].prenom==fiche.prenom) { 
            //parcourt les c fiches
            //regarde si une fiche avec mm prenom et nom que la fiche passée en argument est déjà présente dans la classe
            return true; //si présente
        }
    }
    return false; //si n'est pas présente
}


void saisirClasse(Classe *classe) { //"lireClasse" dans le sujet
    initialiserClasse(classe); //on alloue de la mémoire pour la classe
    int c = (*classe).capacite; //capacité de la classe

    for (int i=0; i<c; i++) { //on parcourt les c emplacements de la classe
        Fiche *ficheC = new Fiche; // on crée l'allocat° pour une nouvelle fiche, ficheC 
        if (ficheC==nullptr) { 
            cout << "-1 : ERREUR D'ALLOCATION" << endl; //code d'erreursi l'allocation a échoué (si ficheC = pointeur nul)
        }
        else { 
            saisirFiche(ficheC); //on demande à l'utilisateur de saisir une fiche
            if (!(dejaPresente(*classe, *ficheC))) { //si elle n'est pas dans la classe...
                classe->fiches[i]=*ficheC; //... on l'ajoute à l'emplacement ficheC pré-établi
            }
        }
    }
}

void afficherClasse(Classe classe) { //"écrireClasse" dans le sujet
    int t = classe.taille; 
    if (t==0) { //si la taille de la classe est nulle
        cout << "Aucun élève." << endl;
    }
    for (int i=0; i<t; i++) { //on parcourt la classe
        cout << "----- ELEVE N° " << i+1 << " ----- " << endl;
        afficherFiche(classe.fiches[i]);
    }
}

void ajouterFiche(Classe *classe, Fiche fiche) {
    int t = classe->taille; 
    int c = classe->capacite;
    if (c=t) { //si la taille égale déjà la capacité
    }

}