//fichier d'entête
#include <iostream>
#include <cmath>
using namespace std;

const int MAXNOTES = 4;

//definition des structures Date, Fiche et Classe
struct Date { 
    int jour;
    string mois;
    int annee;
} ;

struct Fiche { 
    string nom;
    string prenom;
    Date dateDeNaissance;
    float notes[MAXNOTES];
    int nbNotes;
} ;

struct Classe { 
    Fiche *fiches; // champ fiches = (tableau dynamique) pointeur sur la fiche du 1er étudiant de la classe.
    int taille;
    int capacite;
} ;

//prototypage des fonctions
void afficherDate(Date date);
void saisirDate(Date *date);

void saisirFiche(Fiche *fiche);
void ajoutNote(Fiche *fiche);
void afficherFiche(Fiche fiche);

void initialiserClasse(Classe *classe);
bool dejaPresente(Classe classe, Fiche fiche);
void saisirClasse(Classe *classe);
void afficherClasse(Classe classe);

void ajouterFiche(Classe *classe, Fiche fiche);