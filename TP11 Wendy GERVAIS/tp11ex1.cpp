#include "tp11ex1.h"

int main() {
    // on crée 
    cellule *lst=nullptr;
    insereTete(&lst, 8);
    insereTete(&lst, 3);
    insereTete(&lst, 42);
    afficheListe(lst);
    cout << longueurListe(lst) << endl;
    return 0;
}

// insère une nouvelle cellule newcell contenant n au début de la liste lst
void insereTete(Liste *lst, int n) {
    cellule* newcell = new cellule;
    if (newcell == nullptr) {
        cout << "Erreur d'allocation" << endl;
    }
    newcell->valeur = n;
    newcell->suivante = *lst;
    *lst = newcell;
}

void afficheListe(Liste lst) {
    while(lst != nullptr) {
        cout << lst->valeur << endl;
        lst = lst->suivante;
    }
}

int longueurListe(Liste lst) {
    int i=0;
    while(lst != nullptr) {
        i++;        
        lst = lst->suivante;
    }
    return i;
}


