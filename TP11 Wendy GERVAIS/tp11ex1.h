#include <iostream>
using namespace std;

// définition structures
struct cellule {
    int valeur ;
    struct cellule *suivante ;
} ;

typedef cellule* Liste;


// prototypage fonctions
void insereTete(Liste *lst, int n);
void afficheListe(Liste lst); 
int longueurListe(Liste lst);
