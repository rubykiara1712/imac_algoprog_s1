#include "tp11ex2.h"

int main() {
    // on crée 
    cellule *lst=nullptr;
    insereTete(&lst, 5);
    insereTete(&lst, 4);
    insereTete(&lst, 3);
    insereTete(&lst, 2);
    insereTete(&lst, 1);
    afficheListe(lst);
    cout << "Longueur de la liste : " << longueurListe(lst) << endl;
    cout << "Total inférieur à 10 : " << nbInferieurs(lst, 10) << endl;
    cout << "11 est-il présent ? " << recherche(lst, 11) << endl;
    cout << "100 est-il présent ? " << recherche(lst, 100) << endl;
    cout << "Adresse du minimum :  " << minimum(lst) << endl;
    cout << "Adresse de 3 : " << lst->suivante->suivante->suivante->suivante << endl;
    cout << "Triée ? " << estTriee(lst) << endl;
    return 0;
}

// insère une nouvelle cellule newcell contenant n au début de la liste lst
void insereTete(Liste *lst, int n) {
    cellule* newcell = new cellule;
    if (newcell == nullptr) {
        cout << "Erreur d'allocation" << endl;
    }
    newcell->valeur = n;
    newcell->suivante = *lst;
    *lst = newcell;
}

void afficheListe(Liste lst) {
    while(lst != nullptr) {
        cout << lst->valeur << endl;
        lst = lst->suivante;
    }
}

int longueurListe(Liste lst) {
    int i=0;
    while(lst != nullptr) {
        i++;        
        lst = lst->suivante;
    }
    return i;
}

int nbInferieurs(Liste lst, int n) {
    int count=0;

    while (lst != nullptr) {
        if ((lst->valeur)<=n) {
            count++;
        }
        lst = lst->suivante;
    }
    return count;
}

Liste recherche(Liste lst, int n) {
    while (lst != nullptr) {
        if ((lst->valeur)==n) {
            return lst;
        }
    lst = lst->suivante;
    }
    return NULL;
}

Liste minimum(Liste lst) {
    int min = lst->valeur;
    Liste res = lst;

    while (lst != nullptr) {
    if ((lst->valeur)<= min) {
        min = lst->valeur;
        res = lst;
      }
    lst = lst->suivante;
    }
    return res;

}

bool estTriee(Liste lst) {

    while (lst != nullptr && lst->suivante !=nullptr ) { 
        if (lst->valeur > lst->suivante->valeur) {
            return false;
        }
        lst = lst->suivante;
    }
    return true;
}