// TP2 exercice 3
#include <iostream>
using namespace std;

int main() {
    int n1, n2, n3, n4 ;
    cout << "Saisissez 4 entiers : " ; // on demande 4 entiers
    cin >> n1 >> n2 >> n3 >> n4 ; //4 cin dans une seule ligne 
    cout << n1 << endl << n2 << endl << n3 << endl << n4 << endl ; //affiche les 4 entiers ligne par ligne
    cout << "somme = " <<  n1+n2+n3+n4 <<", " << "et moyenne = " << float (n1+n2+n3+n4)/4 << endl ;
    //affiche la somme et la moyenne des 4 entiers. calcul directement dans le cout => moins de lignes
    return 0 ;
    }
