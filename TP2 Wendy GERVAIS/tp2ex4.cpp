// TP2 exercice 4

#include <iostream>
using namespace std;

// 1er programme : demande un entier, affiche s'il est positif ou négatif

int main() {
    int nombre ;
    cout << "Saisissez un entier : " ;
    cin >> nombre ;
    if (nombre >= 0) {
        cout << "Cet entier est positif (ou nul)" << endl ;
    }
    else {
        cout << "Cet entier est négtif" << endl ;
    }
    return 0 ;
    }

/* // 2nd programme : demande un entier, affiche s'il est pair ou impair

int main() {
    int nombre ;
    cout << "Saisissez un entier : " ;
    cin >> nombre ;
    if (nombre %2 == 0) {
        cout << "Pair" << endl ;
    }
    else {
        cout << "Impair" << endl ;
    }
    return 0 ;
    } */