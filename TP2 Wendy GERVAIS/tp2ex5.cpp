// TP2 exercice 5

#include <iostream>
using namespace std;

int main() {
    int a, b, c ;
    cout << "a = "  ;
    cin >> a ;
    cout << "b = " ;
    cin >> b ;
    //demande à l'utilisateur la valeur de deux entiers a et b

    if (a <= b) { //SI a est inférieur à b, on échange leurs valeurs 
        c = a; //on stocke la valeur de a dans une variable c
        a = b; //on affecte la valeur de b à a
        b = c;  //on affecte la valeur de a à b via c

    }
    else { //SINON on incrémente de 10 la variable b 
        b += 10;
    }

    cout <<"a = " << a << " et "<< "b = " << b << endl ; //on affiche les valeurs de a et b
    return 0;
}



