// TP2 exercice 6

#include <iostream>
using namespace std;

//On convertit un nombre de jours J en siècles/années/mois/semaines/jours

int main() {
    int J, sc, a, m, sm, Ji ;
    // entiers qui vont servir à stocker le nombre de jours initial, puis siècles, années, mois, semaines, jours
    cout << "Nombre de jours : " << endl ;
    cin >> J ;
    Ji = J ;
    //on demande un nombre de jours à l'utilisateur et on le conserve dans Ji

    sc = J /36000 ;
    J = J % 36000 ; // 1 siècle = 36000jours

    a = J /360 ;
    J = J % 360 ; // 1 an = 360jours
    m = J /30 ;
    J = J % 30 ; // 1 mois = 30jours
    sm = J /7 ;
    J = J % 7 ; // 1 semaine = 7jours 
    /* On fait les divisions euclidiennes du nombre de jours J par les entités, pour le décomposer. 
    on réaffecte aux entités le quotient, et à J le reste */
    
    string Ji2, sc2, a2, sm2, J2;
    if (Ji>1) { Ji2="jours correspondent à : "; } else { Ji2 = "jour correspond à : ";}
    if (sc>1) { sc2="siècles"; } else { sc2= "siècle";}
    if (a>1) { a2 ="années"; } else { a2= "année";}
    if (sm>1) { sm2 ="semaines"; } else { sm2 = "semaine";}
    if (J>1) { J2="jours";} else { J2= "jour";}
    //permet d'accorder en nombre selon si chaque entité est singulière (0, 1) ou plurielle (>1)

    cout << Ji << "" << Ji2
    << sc << " " << sc2 <<" "
    << a << " " << a2 << " "
    << m << " mois "
    << sm << " " << sm2 << " "
    << J << " " << J2 << " " << endl ;
    //on affiche le résultat final

    return 0 ;
}
