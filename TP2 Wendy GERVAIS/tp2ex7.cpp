// TP2 exercice 7

#include <iostream>
#include <cmath> //pour utiliser l'arrondi avec la fonction round()
using namespace std;

int main() {
    float nb ;
    cout << "Donnez un float positif : " ;
    cin >> nb ;
    cout << (int) round(nb) << endl ; //on convertit nb en un int en en faisant l'arrondi et on l'affiche
    return 0 ;
}