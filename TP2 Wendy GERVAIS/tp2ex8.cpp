// TP2 exercice 8
#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

// 1er programme : demande une chaine et une position et indique le type du caractère à cette position.
/* int main() {
    string chn;
    cout << "Entrez une chaine de caractères (sans espace) : ";
    cin >> chn; //on demande à l'utilisateur d'entrer 1 chaine de caractères

    int position;
    cout << "Entrez une position : ";
    cin >> position; //on demande à l'utilisateur d'entrer 1 position

    position-=1; //ici on nous dit que le premier caractère de la chaine est celui de rang 1, et non 0, donc on décale

    if (position >= (int) chn.length()) { //on teste si la position existe dans la chaine en la comparant à la longueur de la chaine
        cout << "Le caractère à la " << position+1 <<"ème position n'existe pas." << endl;
        }

    else { 
        cout << "Le caractère à la " << position+1 <<"ème position existe !" << endl;

        //si le caractère à la position indiquée (chn.at(position)) existe, on teste via son code ASCII (int (char)) si c'est une lettre/chiffre/autre

        if (int (chn.at(position)) >= 65 && int (chn.at(position)) <= 90) {
            //^ ASCII dec des lettres Maj
            cout << chn.at(position) << " " << "est une lettre majuscule"<< endl; 

        }
        if (int (chn.at(position)) >= 97 && int (chn.at(position)) <= 122) { 
            //^ ASCII dec des lettres min
            cout << chn.at(position) << " " << "est une lettre minuscule"<< endl; 
            }

        if (int (chn.at(position)) >= 48 && int (chn.at(position)) <= 57) { 
            //^ ASCII dec des chiffres
            cout << chn.at(position) << " "  << "est un chiffre"<< endl; 
            }
        
        if ( (int (chn.at(position)) <= 47) || 
            ( (int (chn.at(position)) >= 58) && (int (chn.at(position)) <= 64) ) || 
            (int (chn.at(position)) >= 123)) { 
            //^ ASCII dec des autres
            cout << chn.at(position) << " " << "est un autre type de caractère" << endl;
            }
        }
    return 0 ;

} */


// 2nd programme : jeu enfant adulte
int main() {
    string chn;
    int position;
    cout << "ADULTE : entrez une chaine et une position : ";
    cin >> chn >> position ; //on demande à l'adulte d'entrer la chaine et la position
    position-=1; 

    string rep; //on initialise la variable qui contiendra la bonne réponse au jeu (l/c/a)
    string repenf; //on initialise la variable qui contiendra la réponse de l'enfant (l/c/a)

    if (position >= (int) chn.length()) { //on teste si la position existe dans la chaine en la comparant à la longueur de la chaine
    cout << "Attention, le caractère à la " << position+1 <<"ème position n'existe pas." << endl;
        }

    else { 
        if (int (chn.at(position)) >= 65 && int (chn.at(position)) <= 90) {
            rep = "l";

        }
        if (int (chn.at(position)) >= 97 && int (chn.at(position)) <= 122) { 
            rep = "l";
            }

        if (int (chn.at(position)) >= 48 && int (chn.at(position)) <= 57) { 
            rep = "c";
            }
        
        if ( (int (chn.at(position)) <= 47) || 
            ( (int (chn.at(position)) >= 58) && (int (chn.at(position)) <= 64) ) || 
            (int (chn.at(position)) >= 123)) { 
            rep = "a";
            }
        }
    //selon le type du caractère à la position donnée, on stocke la bonne réponse dans rep

    cout << "ENFANT : quel est le type du caractère " << position+1 <<" ? (l/c/a) " << endl;
    cin >> repenf ; //on demande la réponse à l'enfant
    if (rep==repenf) { cout << "bravo" << endl;}
    else { cout << "La réponse est fausse" << endl;}
    // on affiche si sa réponse est bonne
    return 0 ;

} 
