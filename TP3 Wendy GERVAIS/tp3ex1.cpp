//TP3 exercice 1

#include <iostream>
using namespace std;

int main() {
    int n=0; //on initialise un compteur qui va aller de 1 à 50
    //affiche les entiers entre 1 et 50 (boucle while)
    while(n<50) { 
        n+=1;
        cout << n << endl;
    }

    n=0;  //on ré-initialise le compteur
    //affiche les 50 premiers multiples de 7 (boucle while)
    while(n<50) { 
        cout << n << " fois 7 est " << n*7 << endl;
        n+=1;
    }

    //affiche les entiers entre 1 et 50 (boucle for)
   for (int i=1; i<=50; i++) { 
        cout << i << endl;
    }

    //affiche les 50 premiers multiples de 7 (boucle for)
    for(int i=0; i<50; i++) { 
        cout << i << " fois 7 est " << i*7 << endl;
        n+=1;
    }
return 0;
}



