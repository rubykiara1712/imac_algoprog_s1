//TP3 exercice 2

#include <iostream>
using namespace std;

int main() {
    cout << "Saisissez des entiers : " ; // on demande des entiers
    int n, i, s; 
    n=0;
    while (n >= 0){ // tant que l'utilisateur entre des nombre positifs
        cin >> n ; // on demande un nouvel entier
        s = s+ n; // on rajoute l'entier à la somme
        i += 1; // on incrémente le compteur (va servir à calculer la moyenne car nécéssite de connaitre le nombre d'entiers entrés)
    } 
    float moy;
    moy= s/i;
    cout << "somme = " << (int) s <<", " << "et moyenne = " << moy << endl ;
    //affiche la somme et la moyenne des entiers
    
    return 0 ;
    }