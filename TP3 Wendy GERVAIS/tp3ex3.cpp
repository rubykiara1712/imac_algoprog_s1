//TP3 exercice 3

#include <iostream>
using namespace std;

int afficheEntiers(int n); // Affiche tous les entiers entre 1 et n 
int ajoute3 (int m); // RENVOIE le nombre +3

int main() {
    int n;
    cout << "Entrez un nombre : " ;
    cin >> n ; //on laisse l'utilisateur choisir un nombre
    afficheEntiers(n); //appel à la fonction afficheEntiers
    cout << ajoute3(n) << endl; //affiche le retour de la fonction ajoute3 de n

return 0;
}

int afficheEntiers(int n) {
    for (int i=1; i<=n; i++) { 
        cout << i << endl; //boucle qui permet d'afficher i pour i allant de 1 à n
    }
return 0;
}

int ajoute3(int m) {
    return (m+3); //retourne m incrémenté de 3
}