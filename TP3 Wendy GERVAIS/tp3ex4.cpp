//TP3 exercice 4

#include <iostream>
using namespace std;

int clef (int n) ; //fonction qui calculera la clé
bool verif(int n, int c); //fonction qui vérifie si le nombre correspond à la clé entrée

int main() {
    int n, c ;
    cout << "Entrez un entier : " ;
    cin >> n ;
    cout << "Entrez la clé associée : " ;
    cin >> c ;
    if (verif(n, c)) { cout << "Vrai : tu as gagné" << endl ;} //l'utilisateur a gagné si la fonction vérif a renvoyé vrai
    else { cout << "Faux : tu es puni" << endl; } //l'utilisateur a perdu sinon
    
    
return 0;
}

int clef (int n) { //fonction qui calcule la clé associée à un nombre par divisions euclidiennes
    int reste = 0, cle = 0 ; //on initialise un reste et la clé
    do {
        reste = n%10;
        n = n/10; //on effectue la division euclidienne de n par 10 pour passer de puissance de 10 en puissance de 10
        cle += reste; //on somme les restes
    } while (n >= 1);
    return cle ;
}

bool verif(int n, int c) {
    return (c==clef(n)); //renvoie vrai si la clé entrée correspond à la clé calculée
}
