//TP3 exercice 5

#include <iostream>
#include <string>
using namespace std;

//une fonction de hachage affecte une "signature numérique" à une donnée. 
//Ici le modulo 100 de la somme des codes ASCII des caractères d'une chaine

int hachage(string n) ;

int main() {
    string n;
    cout <<  "Entrez une chaine de caractère : " << endl;
    cin >> n ; //demande à l'utilisateur d'entrer une chaine
    cout <<  hachage(n) << endl; //retourne le hachage de cette chaine grâce à la fonction hachage
return 0;
}

int hachage(string n) {
    int code=0, i=0; //on initialise le code ainsi qu'un compteur
    int longueur = n.length(); //on stocke la longueur de la chaine n 
    while (i <= longueur) { 
        code += (int)n[i]; // on parcourt les caractères de la chaine et on somme leurs codes ASCII 
        i+=1; 
    } 
return code%100 ; //on retourne le code[100]
}