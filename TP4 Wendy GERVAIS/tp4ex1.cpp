#include <iostream>
#include <string>
using namespace std;

int main() {
    int MAXTAILLE=5;
    int tab1[MAXTAILLE]; //on déclare un tableau tab1 de taille MAXTAILLE
    cout << "La valeur tab1[5] = " << tab1[5]<< endl; //on affiche la 6eme valeur (position 5) de tab1
    for (int i=0; i<5; i++){ 
        tab1[i] = i+6; //on remplit tab1 avec 6, 7, 8, 9, 10
        }
    cout << "La valeur tab1[3] = " << tab1[3]<< endl; //on affiche la valeur 3 de tab1
    int tab2[] = {6, 7, 8, 9, 10}; //on déclare et remplit un nouveau tableau tab2
    cout << "La valeur tab2[3] = " << tab2[3]<< endl; //on affiche la valeur 3 de tab2
return 0;

}
