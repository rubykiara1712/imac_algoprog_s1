#include <iostream>
using namespace std;

int main() {
    int n = 1;  //valeur de n
    int* p = &n; //pointeur p = adresse de n
    cout << "L'adresse de n est " << &n <<  endl; //on affiche l'adresse de n avec &
    cout << "L'adresse de n est " << p << endl; //on affiche l'adresse de n avec p
    cout << "La valeur de n est " << n << endl; //on affiche la valeur de n
    *p = 2; //on réaffecte 2 à n en passant par p
    cout << "La nouvelle valeur de n est " << n << endl; //on affiche la nouvelle valeur de n
    return 0;
}