#include <iostream>
using namespace std;

int  incremente(int m);
int  incrementeParAdresse(int* p);
void echange(int* a, int* b);


int main() {
    /*int n = 1;
    cout << "La valeur est " << n << endl;
    cout << "L'adresse est " << &n << endl;
    incrementeParAdresse(&n);
    cout << "La valeur est " << n << endl;*/  
    
    //première fonction : appel à Incrémente Par Adresse

    int a=5, b=6 ; //test avec 5 et 6
    echange(&a, &b);//appel à l'échange de a et b en passant par leurs adresses par la fonction échange
}

int  incremente(int m) {
    m += 1; //incrémentation
    //affichage valeur et adresse
    cout << "La valeur est " << m << endl;
    cout << "L'adresse est " << &m << endl;
return 0;

}

int  incrementeParAdresse(int* p) {
    *p += 1; //incrémentation
    //affichage valeur et adresse
    cout << "La valeur est " << *p << endl;
    cout << "L'adresse est " << &p << endl;
return 0;
}

void echange(int* a, int* b) {
    cout << "La valeur de a est " << *a << endl;
    cout << "La valeur de b est " << *b << endl; //premier affichage de a et b
    int c = *a;
    *a = *b;
    *b = c; //échange des valeurs de a et b par leurs pointeurs en passant par une variable c 
    cout << "La valeur de a est " << *a << endl;
    cout << "La valeur de b est " << *b << endl; //affichage de a et b après échange
    
}