#include <iostream>
using namespace std;

void quotientEtReste (int divis, int divid, int *quotient, int *reste);

int main() {
    int dividende, diviseur, quotient, reste;
    cin >> diviseur >> dividende; //demande le diviseur et le dividende à l'utilisateur
    quotientEtReste (diviseur, dividende, &quotient, &reste); //appel à la fonction qui calcule le quotient et le reste et les stocke dans quotient et reste de la main()
    cout << dividende <<" = "<< quotient << " * " << diviseur <<" + "<< reste << endl; //affiche le résultat de la division euclidienne
    return 0;
}

void quotientEtReste (int divis, int divid, int *quotient, int *reste) {
    *quotient = divid/divis; // on change la valeur pointée par quotient (on y met le quotient de la division euclidienne)
    *reste = divid%divis; // on change la valeur pointée par reste (on y met le reste de la division euclidienne)
    }
