#include <iostream>
using namespace std;

int quotientEtReste (int divis, int divid, int *quotient, int *reste);

int main() {
    int dividende, diviseur, quotient, reste;
    cin >> diviseur >> dividende; //demande le diviseur et le dividende à l'utilisateur
    quotientEtReste (diviseur, dividende, &quotient, &reste); //appel à la fonction qui calcule le quotient et le reste et les stocke dans quotient et reste de la main()

    if (quotientEtReste (diviseur, dividende, &quotient, &reste)==1) {cout << dividende <<" = "<< quotient << " * " << diviseur <<" + "<< reste << endl;} //si la fonction a renvoyé 1 on effectue la division
    else {cout << "DIVISION PAR 0 IMPOSSIBLE !"<<endl;} //sinon, on affiche l'erreur
    return 0;
}

int quotientEtReste (int divis, int divid, int *quotient, int *reste) {
    if (divis == 0) {return 0;} //on renvoie 0 si la division est impossible
    else { *quotient = divid/divis; // on change la valeur pointée par quotient (on y met le quotient de la division euclidienne)
        *reste = divid%divis; // on change la valeur pointée par reste (on y met le reste de la division euclidienne)
        return 1; }
}
