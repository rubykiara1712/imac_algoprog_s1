//TP5 exercice1

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

int main() { 
    char lettre='t';
    cout << lettre << endl; //on affiche lettre en tant que caractère
    cout << (int) lettre << endl ; //on affiche lettre en tant qu'entier = son code ASCII
    lettre +=2 ; //on augmente lettre de 2
    cout << lettre << endl; //nouvel affichage
    cout << (int) lettre << endl ;

    //affichage des caractères de A jusqu'à Z et 0 à 9, avec leurs codes ASCII
    lettre='A';
     while (lettre != 'Z' +1) {
        cout << lettre << ", " << (int) lettre << endl;
        lettre+=1;
    }
    char chiffre='0';
    while (chiffre != '9' + 1) {
        cout << chiffre << ", " << (int) chiffre << endl;
        chiffre+=1;
    }

    return 0;
}