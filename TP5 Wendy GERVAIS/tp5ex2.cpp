//TP5 exercice2

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

void afficheChaine(char chaine[]);

int main() { 
    char chaine[256];
    cout << "Entrez chaine :" << endl;
    cin >> chaine; //demande une chaine de caractères à l'utilisateur
    afficheChaine(chaine); //affiche la chaine ; /!\ s'arrête s'il y a un espace !
    return 0;
}

void afficheChaine(char chaine[]) {
    int i = 0;
    while (chaine[i]!='\0') { //fonction qui parcourt la chaine et affiche ses caractères : le caractère \0 termine la chaine
        cout << chaine[i] ;
        i+=1;
    }
    cout << endl;
}