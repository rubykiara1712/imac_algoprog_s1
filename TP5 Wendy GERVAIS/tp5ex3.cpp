//TP5 exercice3

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

int longueurChaine(char chaine[]);
void copieChaine(char dest[], char src[]);
void afficheChaine(char chaine[]);

int main() { 
    char chaine[256];
    char chaine2[256];
    cout << "Entrez chaine :" << endl;
    cin >> chaine;
    cout << "Entrez chaine 2 :" << endl;
    cin >> chaine2;
    cout << "La longueur de chaine est " << longueurChaine(chaine) << " ou encore " << strlen(chaine) << endl; //on affiche la longueur calculée avec longueurChaine puis avec strlen

    //copieChaine(chaine, chaine2); //on copie chaine2 dans chaine avec la fonction copieChaine
    strcpy(chaine, chaine2); //on copie chaine2 dans chaine avec strcpy
    afficheChaine(chaine); 
    afficheChaine(chaine2); //on affiche les 2 nouvelles chaines après copie
    return 0;
} 

int longueurChaine(char chaine[]) {
    int i = 0;
    while (chaine[i]!='\0') { //parcourt la chaine (le caractère \0 termine la chaine)
        i+=1;
    }
return i; //renvoie la dernière position atteinte  = la longueur de la chaine
}

void copieChaine(char dest[], char src[]) { //fonction copieChaine qui copie une chaine src dans une chaine dest
    int LS = longueurChaine(src); 
    for (int i=0; i<=LS; i++) {
        dest[i]=src[i]; //on affecte à chaque élément de dest l'élement de src en même position
    }
}


void afficheChaine(char chaine[]) { //fonction qui affiche la chaine (exo 2)
    int i = 0;
    while (chaine[i]!='\0') { //fonction qui parcourt la chaine et affiche ses caractères : le caractère \0 termine la chaine
        cout << chaine[i] ;
        i+=1;
    }
    cout << endl;
}


