//TP5 exercice3 (bis)

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

int compteChiffres(char chaine[]);
int longueurChaine(char chaine[]); //on a besoin de longueurChaine pour compteChiffres

int main() { 
    char chaine[256];
    cout << "Entrez chaine :" << endl;
    cin >> chaine; //on demande une chaine à l'utilisateur
    cout << "La chaine contient " << compteChiffres(chaine) << " chiffres." << endl; //on renvoie le nb de chiffres de la chaine
    return 0;
} 

int compteChiffres(char chaine[]) { //compte le nombre de chiffres dans une chaine
    int nb = 0; //on initialise un compteur
    int L = longueurChaine(chaine);
    for (int i=0; i<=L; i++) {
        if ((int) chaine[i] >= 48 && (int) chaine[i] <= 57) { 
            nb +=1; //incrémente le compteur nb si un chiffre est rencontré (repéré par son code ASCII compris entre 48 et 57)
            }
    }       
    return nb;

}

int longueurChaine(char chaine[]) {
    int i = 0;
    while (chaine[i]!='\0') { //parcourt la chaine (le caractère \0 termine la chaine)
        i+=1;
    }
return i; //renvoie la dernière position atteinte  = la longueur de la chaine
}

