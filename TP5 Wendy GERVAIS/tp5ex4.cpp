//TP5 exercice4

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

long int fact(int n);
int pgcdEuc(int A, int B);


int main() { 
    int a;
    int b;
    cout << "Entrez a et b :" << endl;
    cin >> a >> b;
    cout << "Le PGCD de " << a << " et " << b <<  " est " << pgcdEuc(a, b) << "." << endl;
//demande deux entiers et affiche leur PGCD
}

long int fact(int n) {  //fonction récursive qui renvoie la factorielle d'un entier grâce à la relation de récurrence n! = n*(n-1)!
    if (n==0) {return 1;}
    else {return n*fact(n-1);}
}

int pgcdEuc(int A, int B) { //algorithme d'Euclide : fonction récursive qui renvoie le PGCD de deux entiers
   if (B==0) {return A;}
   else { return pgcdEuc(B, A%B);}
}