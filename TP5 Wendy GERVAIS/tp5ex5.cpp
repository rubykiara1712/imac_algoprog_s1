//TP5 exercice5

#include <iostream>
#include <string.h>
#include <cstring>
#include <cmath>
using namespace std;

double suiteU(int n);
double suiteV(int n);
int n1=0, n2=0; //compteurs initialisés

int main() { 
    int n;
    cout << "Entrez 1 entier : " ;
    cin >> n; //on demande un entier n à l'utilisateur
    cout << "U_" << n << " = " << suiteU(n) << " et V_"  << n << " = " << suiteV(n) << endl; //affiche U_n et V_n
    cout << "suiteU a été appelée " << n1 << " fois et suiteV " << n2 << " fois. " << endl;
    //donne le nombre d'appels aux fonctions U et V
    return 0;
}

double suiteU(int n) {
    n1++; //on incrémente le compteur n1 lié à suiteU à chaque appel de celle-ci
    if (n==0) {
        return 2;
    }
    else {
        return ((suiteU(n-1) + suiteV(n-1))/2);
        // ou : return (suiteU(n-1)/2 + 1); si l'on remplace V_n par 2
    } //fonction récursive qui calcule U_n

}

double suiteV(int n) {
    n2++; //on incrémente le compteur n1 lié à suiteV à chaque appel de celle-ci
    if (n==0) {
        return 1;
    }
    else {
        return (2/suiteU(n));
    } //fonction récursive qui calcule V_n

} 