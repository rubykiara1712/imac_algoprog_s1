//TP5 exercice 6

#include <iostream>
#include <string.h>
#include <cstring>
#include <cmath>
using namespace std;

long int pssc(int n, int p);
int c=0; //compteur initialisé

int main() { 
    int n;
    cout << "Entrez n : " ;
    cin >> n; //on demande un entier n à l'utilisateur

    int p;
    cout << "Entrez p : " ;
    cin >> p; //on demande un entier p à l'utilisateur

    cout << n << "^" << p << " vaut " << pssc(n,p) << endl; //renvoie n puissance p
    cout << "La fonction pssc a été appelée " << c << " fois." << endl; //affiche le nombre de fois que pssc a été appelée (= p+1 fois)

    return 0;
}

long int pssc(int n, int p) {
    c++; //on incrémente le compteur à chaque passage dans la fonction pssc
    if (p==0) {return 1;}
    else { return n*pssc(n, p-1); } //fonction récursive permettant de calculer n^p avec la relation n^p = n*n^(p-1)
}

