//TP5 exercice 7

#include <iostream>
#include <string.h>
#include <cstring>
#include <cmath>
using namespace std;

void minMaxTab(int tab[], int* m, int* M);
int chercheEntier(int tab[], int n);

int main() {  
    int tab[6]={1, 5, 6, 5, 2, 9};
    int min, max;
    minMaxTab(tab, &min, &max); //on initialise min et max dans main, on applique la fonction  aux &!!!
    cout << "Le  minimum est : " << min << endl;
    cout << "Le  maximum est : " << max << endl; //renvoie le minimum et le maximum du tableau tab

    cout << "3 apparaît en position " << chercheEntier(tab, 3) << endl; //test en cherchant 3 dans tab (renvoie -1 car il n'y est pas)
    cout << "5 apparaît en position " << chercheEntier(tab, 5) << endl; //test en cherchant 5 dans tab (renvoie 1)

    return 0;
}

void minMaxTab(int tab[], int* m, int* M){ //mettre les deux valeurs à renvoyer en ARGUMENT afin de les récupérer dans le main!!!!
    *m=tab[0], *M=tab[0];
    for (int i=0; i< 6; i++){
        if (tab[i]<=*m) {
            *m = tab[i]; //parcourt le tableau et trouve le minimum et le met dans l'adresse pointée par m
        }
        if (tab[i]>=*M) {
            *M = tab[i]; //parcourt le tableau et trouve le maximum et le met dans l'adresse pointée par M
        }
    }
}

int chercheEntier(int tab[], int n) { //cherche et renvoie la première occurrence de n dans tab
    int indx;
    for (int i=0; i< 6; i++){ //parcourt les éléments du tableau
        if (tab[i]==n) {
            indx = i; //si un élément du tableau est n, on renvoie directement sa position
            return indx;
        }
        else {
            indx = -1; //sinon, indx devient -1
        }
    }
    return indx; //ainsi si n n'a jamais été trouvé on renvoie -1

}