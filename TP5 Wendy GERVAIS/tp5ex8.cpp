//TP5 exercice 8

#include <iostream>
#include <string.h>
#include <cstring>
#include <cmath>
using namespace std;

void inverseChaine(char chaine[]);
void enleveMajuscule(char chaine[]);

int main() {  
    char chaine[256];
    cout << "Entrez 1 chaine : " << endl;
    cin >> chaine ; //demande une chaine à l'utilisateur
    //inverseChaine(chaine); //affiche la chaine inversée
    enleveMajuscule(chaine); //enlève les majuscules de la chaine
    return 0;
}

void inverseChaine(char chaine[]) {
    int size = 0;
    while (chaine[size]!='\0') {
        size+=1; //récupère la taille size de la chaine
    }

    char chaineI[256]; //crée une seconde chaine, chaine I, qui sera chaine inversée

    int i2=0;
    for (int i=size-1; i>=0; i--) { //parcourt la chaine à l'envers (de size-1 (!!!!!) à 0)
        chaineI[i2]=chaine[i]; //met les éléments de chaine à l'envers dans chaineI
        i2++;
    }

    int j = 0;
    while (chaineI[j]!='\0') { //boucle while permettant d'afficher la chaine chaineI 
        cout << chaineI[j] ;
        j+=1;
    }
cout << endl;
}

void enleveMajuscule(char chaine[]) {
    int size = 0;
    while (chaine[size]!='\0') {
        size+=1; //récupère la taille size de la chaine
    }

    char chaine2[256]; //crée une seconde chaine, chaine2, qui sera chaine sans les majuscules
    int i2=0;
    for (int i=0; i<size; i++) { //parcourt la chaine
        if ((int) chaine[i] <= 63 || (int) chaine[i] >= 91) { //si le code ASCII du i-eme élément de chaine n'est PAS celui d'une majuscule
            chaine2[i2] = chaine[i]; // alors on l'ajoute à chaine2
            i2++; //incrémentation de i2 pour avancer dans chaine2
        }
    }

    int j = 0;
    while (chaine2[j]!='\0') { //boucle while permettant d'afficher la chaine chaine2
        cout << chaine2[j] ;
        j+=1;
    }
cout << endl;

}