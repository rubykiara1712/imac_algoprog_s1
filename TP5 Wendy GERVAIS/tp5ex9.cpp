//TP5 exercice 9

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

/* Recherche en ligne
https://www.cplusplus.com/reference/cstring/strncmp/
int strncmp ( const char * str1, const char * str2, size_t num );
Compare characters of two strings
Compares up to num characters of the C string str1 to those of the C string str2.
This function starts comparing the first character of each string. If they are equal to each other, 
it continues with the following pairs until the characters differ, until a terminating null-character is reached, 
or until num characters match in both strings, whichever happens first.

strcmp compare lexicalement les deux chaînes caractère par caractère et renvoie 0 si les deux chaînes sont égales, 
un nombre positif si s1 est lexicographiquement supérieure à s2, et un nombre négatif si s1 est lexicographiquement inférieure à s2. */

int comparaisonstr(char* s1, char* s2);

int main() {  
    char c1[256];
    char c2[256];
    cout << "Entrez 2 chaines : " << endl;
    cin >> c1 >> c2 ; //demande deux chaines à l'utilisateur pour les comparer
    cout << comparaisonstr(c1, c2) << endl; //compare avec ma fonction
    cout << strcmp(c1, c2) << endl; //compare avec strcmp
    return 0;
}

int comparaisonstr(char* s1, char* s2) {
    int i = 0;
    while (s1[i]!='\0' || s2[i]!='\0') { //on parcourt s1 et s2 : on s'arrête dès que la fin de l'une d'elle est atteinte
        if (((int) s1[i]) > ((int) s2[i])) {
            return 1; //renvoie un entier positif si s1 est lexic. supérieure à s2
        }
        if (((int) s1[i]) < ((int) s2[i])) {
            return -1; //renvoie un entier négatif si s1 est lexic. inférieure à s2
        }
    i++; //incrémente le compteur i
    }
    return 0;
    }