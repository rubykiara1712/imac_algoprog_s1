//TP6 exercice 1

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;

void repeter(int argc, char *argv[]);
int multiplier(int argc, char *argv[]);

int main(int argc, char *argv[]) { //main(nb d'arguments, liste des arguments)
    répéter (argc, argv); //appel à la fonction répéter
    cout << multiplier(argc, argv) << endl; //appel à la fonction multiplier : multipliera les deux entiers passés en paramètre (s'il y en exactement 2)
}

void repeter(int argc, char *argv[]) {
    string arg[argc]; //on crée arg = la chaine qui contiendra les arguments

    cout << argc << endl; //affiche le nombre d'arguments

    for (int i=0; i<argc; i++) {
        arg[i] = string(argv[i]); //met les arguments dans arg
    }

    for (int i=0; i<argc; i++) {
        cout << arg[i] << endl; //affiche les éléments de arg = les arguments, ligne par ligne
    }

}

int multiplier(int argc, char *argv[]) {
    int terme=0;
    int produit=1;

    if (argc==3) { //si on entre 2 arguments (+ le nom de l'exécutable)

        for (int i=1; i<argc; i++){
            terme = atoi(argv[i]); //conversion des éléments de argv en entiers avec ATOI
            produit= produit*terme; //on effectue le produit des éléments entrés en paramètre (éléments de argv)
        }
        return produit;
     }
    else {
        return -1; //renvoie -1 s'il y a plus de deux éléments (si on enlève le if/else on pourrait faire le produit d'autant entiers entrés)
    }
    

}