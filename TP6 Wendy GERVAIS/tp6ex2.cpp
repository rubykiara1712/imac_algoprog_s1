//TP6 exercice 2

#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;


int main(int argc, char *argv[]) { 
    char* rep; //on initialise la réponse à la question
    if (argc>3) { 
       cout << "Erreur : trop d'arguments" << endl; // si + de 2 arguments (3 = nom exécutable + 2) entrés : on renvoie un message d'erreur
    }
    else {
        rep = strstr(argv[2], argv[1]); //on affecte à rep la valeur de la fonction strstr appliquée aux deux arguments entrés 
        //si le premier argument entré n'est pas contenu dans le second, rep prend la valeur NULL
        //conditionnelle pour répondre à la question
        if (rep == NULL ) { cout << argv[2] << " ne contient pas "<< argv[1] << endl;}
        else { cout  << argv[2] << " contient " << argv[1] << endl;}
        return 0;
    }
}

