//TP6 exercice 2 bis (nbOccurrences)
#include <iostream>
#include <string.h>
#include <cstring>
using namespace std;


int main(int argc, char *argv[]) { 
    char* rep;
    int c=0; //on initialise un compteur c

    if (argc>3) { 
       cout << "Erreur : trop d'arguments" << endl; // si + de 2 arguments (3 = nom exécutable + 2) entrés : on renvoie un message d'erreur
    }
    else {
        rep = strstr(argv[2], argv[1]); //on initialise rep avec la réponse à "est-ce que le premier arg entré contient le second ?"
        while(rep != NULL) { //tant que le premier élément contient le second....
            rep=strstr(rep+1, argv[1]); //... on ré-affecte rep avec la réponse concernant rep+1 et le premier argument
            c++; //on incrémente le compteur à chaque passage ie quand l'occurrence est trouvée
        }
    }
    cout <<  argv[2] << " contient " << c << " fois " << argv[1] << endl; //on affiche c = le nb d'occurrencesb
    return c;
}
