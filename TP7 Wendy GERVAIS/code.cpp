#include <iostream>
using namespace std;


int puissance(int base, int puis) {
    int result = 1;
    while (puis > 0) {
	result *= base;
	puis -= 1;
    }
    return result;
}


float partie_decimale(float f) {
    return f - (int)f;
}


int main(void) {
    cout <<"3 puissance 5 est " << puissance(3,5)<< endl;
    cout <<"Partie decimale de 4.672 est " << partie_decimale(4.672) << endl;

    return 0;
}
    