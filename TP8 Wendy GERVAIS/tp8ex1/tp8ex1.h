//fichier d'entête
#include <iostream>
#include <cmath>
using namespace std;

//definition de la structure Complexe
struct Complexe { 
    float reel; 
    float img;
} ;

//prototypage des fonctions
void initComplexe(Complexe *c, double reel, double img);
void afficheComplexe(Complexe c);
Complexe sommeComplexe(Complexe c1, Complexe c2);
Complexe produitComplexe(Complexe c1, Complexe c2);
Complexe conjugue(Complexe c);
double moduleComplexe(Complexe c);
Complexe inverseComplexe(Complexe c);
