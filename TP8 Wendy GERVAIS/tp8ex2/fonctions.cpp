#include "tp8ex2.h"

void afficherDate(Date date) { //affiche la date 
    cout << date.jour << " " << date.mois << " " << date.annee << "." << endl;
}

void saisirDate(Date *date) { //demande une date à l'utilisateur
    int j, a;
    string m;
    cout << "jour ? " ;
    cin >> j;
    cout << "mois ? " ;
    cin >> m;
    cout << "annee ? " ;
    cin >> a;

    (*date).jour = j;
    (*date).mois = m;
    (*date).annee = a;
}


void saisirFiche(Fiche *fiche) { //demande à l'utilosateur d'entrer une fiche
    string nom ;
    string prenom;
    Date anniv;
    int nbNotes = 0; //initialisation 

    cout << "nom ? " ;
    cin >> nom;
    cout << "prénom ? " ;
    cin >>  prenom;
    cout << "Date de naissance : " ;
    saisirDate(&anniv); //on demande les éléments de la fiche à l'utilisateur

    (*fiche).nom = nom;
    (*fiche).prenom = prenom;
    (*fiche).dateDeNaissance = anniv;
    (*fiche).nbNotes = nbNotes; //affectation
}

void ajoutNote(Fiche *fiche) {
    float note; //la note qu'on va ajouter

    if ((*fiche).nbNotes == MAXNOTES) { //si le tableau est déjà plein, on ne fait rien.
        return ;
    }
    else {
        cout << "Nouvelle note : " ;
        cin >> note ; //on demande une nouvelle note
        (*fiche).notes[(*fiche).nbNotes] = note; //on l'ajoute en fin de tableau
        (*fiche).nbNotes++; //on incrémente le nombre de notes de la fiche
        }
    }

void afficherFiche(Fiche fiche) {
    cout <<"Nom : " << fiche.nom << endl;
    cout <<"Prénom : " << fiche.prenom << endl;
    cout <<"Date de naissance : " ;
    afficherDate(fiche.dateDeNaissance);
    cout <<"Notes : " ;
    
    if (fiche.nbNotes == 0) {
        cout << "aucune note." << endl;
        return;
    }
    else {
        for (int i=0; i<fiche.nbNotes; i++) { //affichage des notes en parcourant le tableau notes
            cout << fiche.notes[i] << " " ;
        }
    }
}

float Moyenne(Fiche fiche) { //calcule la moyenne des notes
    int Moy=0;
    if (fiche.nbNotes == 0) {
        cout << "Pas de moyenne : aucune note." << endl;
        return -1;
    }
    else {
        for (int i=0; i<fiche.nbNotes; i++) { //parcourt la liste de notes et les sommes
            Moy+=fiche.notes[i];
        }
    }
    Moy = Moy/(fiche.nbNotes); //divise par le nb de notes
    return Moy;
}