//TP8 exercice 2
#include "tp8ex2.h"

/* //Premier programme : le main demande et affiche une date
int main () { 
    Date date;
    saisirDate(&date); //demande à l'utiisateur de saisir une date jour puis mois puis année
    afficherDate(date);  //affiche cette date
    return 0;
} */

//Second programme : le main demande et affiche une fiche
int main () { 
    Fiche fiche;
    saisirFiche(&fiche); //on entre une fiche
    ajoutNote(&fiche); //on entre 5 notes : à l'exécution, il n'en demande que 4 car MAXNOTES=4
    ajoutNote(&fiche);
    ajoutNote(&fiche);
    ajoutNote(&fiche);
    ajoutNote(&fiche);
    afficherFiche(fiche); //on affiche la fiche avec les  3 notes en +
    cout << "Moyenne = " << Moyenne(fiche) << endl;
    return 0;
} 