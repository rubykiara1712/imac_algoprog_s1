//fichier d'entête
#include <iostream>
#include <cmath>
using namespace std;

const int MAXNOTES = 4;

//definition des structures
struct Date { 
    int jour;
    string mois;
    int annee;
} ;

struct Fiche { 
    string nom;
    string prenom;
    Date dateDeNaissance;
    float notes[MAXNOTES];
    int nbNotes;
} ;

//prototypage des fonctions
void afficherDate(Date date);
void saisirDate(Date *date);

void saisirFiche(Fiche *fiche);
void ajoutNote(Fiche *fiche);
void afficherFiche(Fiche fiche);

float Moyenne(Fiche fiche);