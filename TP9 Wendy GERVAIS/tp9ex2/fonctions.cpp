#include "tp9ex2.h"

void initComplexe(Complexe *c, double reel, double img) { //fonction qui initialise un complexe c (via son pointeur!)
   (*c).reel = reel; //on init. partie réelle
   (*c).img = img; //on init. partie imaginaire
   //via pointeur car c existe dans le main !!!!!
}

void afficheComplexe(Complexe c) { //fonction qui AFFICHE un complexe c passé en argument
    if (c.img<0) {cout << c.reel << "-" << abs(c.img) << "*i" << endl;} //pour que ça soit propre si c.img est négatif et pas avoir +-img*i...
    else {cout << c.reel << "+" << c.img << "*i" << endl;}
}

Complexe sommeComplexe(Complexe c1, Complexe c2) { //fonction qui retourne S, la somme de deux complexes c1 et c2 passés en argument
    Complexe S;
    double Sreel = c1.reel + c2.reel ;
    double Simg = c1.img + c2.img;
    initComplexe(&S, Sreel, Simg);
    return S;
    
}

Complexe produitComplexe(Complexe c1, Complexe c2) { //fonction qui retourne P, le produit de deux complexes c1 et c2 passés en argument
    Complexe P;
    double Preel = c1.reel*c2.reel - c1.img*c2.img;
    double Pimg = c1.reel*c2.img + c2.reel*c1.img; 
    initComplexe(&P, Preel, Pimg);
    return P;
}

Complexe conjugue(Complexe c) { //retourne le conjugué de c (opposé de la partie imaginaire)
    Complexe cbarre;
    initComplexe(&cbarre, c.reel, -c.img);
    return cbarre;
}

double moduleComplexe(Complexe c) { //calcule le module sachant module = sqrt(c fois son conjugué)
    double Module;
    Module = sqrt(produitComplexe(c, conjugue(c)).reel);
    return Module;
}

Complexe inverseComplexe(Complexe c) { //calcule l'inverse 
    Complexe Inv ;
    if (moduleComplexe(c)==0) { 
        cout << "Erreur : 0 n'a pas d'inverse !" << endl;
        initComplexe(&Inv, 0, 0);
    }
    else { 
        double coeff = 1/(moduleComplexe(c)*moduleComplexe(c));
        Inv.reel = conjugue(c).reel * coeff;
        Inv.img = conjugue(c).img * coeff;
    }
    return Inv;
}