//TP9 exercice 2
#include "tp9ex2.h"

int main () {
    Complexe z1, z2; //on initialise 2 complexes z1 et z2
    initComplexe(&z1, (double) 0, (double) 0); //z1 = 5 + 10i et z2= 3 + 11i
    initComplexe(&z2, (double) 3, (double) 11); //on les initialise avec initComplexe

    cout << "z1 = ";
    afficheComplexe(z1); //on affiche z1

    cout << "z2 = ";
    afficheComplexe(z2); //on affiche z2

    cout << "z1 + z2 = ";
    afficheComplexe(sommeComplexe(z1, z2)); //on affiche la somme de z1 et z2

    cout << "z1 * z2 = ";
    afficheComplexe(produitComplexe(z1, z2)); //on affiche le produit de z1 et z2

    cout << "z1barre = " ;
    afficheComplexe(conjugue(z1)); //on affiche le conjugué de z1

    cout << "|z1| = " << moduleComplexe(z1) << endl; //on affiche le module de z1

    cout << "1/z1 = ";
    afficheComplexe(inverseComplexe(z1)) ; //on afficge l'inverse de z1

    return 0;
}
