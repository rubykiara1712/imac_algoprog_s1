#include <iostream>
using namespace std;

//include guards
#ifndef FONCTIONS_H
#define FONCTIONS_H


// prototypage des 2 fonctions
int puissance(int base, int puis);
float partie_decimale(float f);

#endif /* FONCTIONS_H */